//2.
db.fruits.aggregate([

    {$match:{$and:[{supplier:"Yellow Farms"},{price:{$lte:50}}]}},
    {$count:"yellowFarmsLowerThan50"}

])
//3.
db.fruits.aggregate([

    {$match:{price:{$lte:30}}},
    {$count: "priceLesserThan30"}

])
//4.
db.fruits.aggregate([

    {$match:{supplier:"Yellow Farms"}},
    {$group:{_id:"$supplier", avgPrice:{$avg: "$price"}}}

])
//5.
db.fruits.aggregate([

    {$match:{supplier:"Red Farms Inc."}},
    {$group:{_id:"$supplier", maxPrice:{$max: "$price"}}}

])
//6.
db.fruits.aggregate([

    {$match:{supplier:"Red Farms Inc."}},
    {$group:{_id:"$supplier", minPrice:{$min: "$price"}}}

])
